VMX_SUPPORT:=$(shell grep vmx /proc/cpuinfo)
RAW_IMG:=image.raw
COW_IMG:=image.qcow
UBUNTU_IMG:=ubuntu.raw
MEMORY:=2G

check: have-kvm have-qemu have-vmx

have-kvm:
	@[ -f /dev/kvm ] || echo "No kvm enabled" && false

have-qemu:
	@which qemu-system-x86_64

create-image:
	qemu-img create -f raw -o size=4G $(RAW_IMG)

partition-image:
	parted $(RAW_IMG) --script "mklabel gpt" "mkpart ext4 0 -1M"

format-image: partition-image
	loop=`losetup -fP --show $(RAW_IMG)`
	mkfs.ext4 /dev/${loop}p1

convert-image:
	qemu-img convert -f raw -O qcow2 $(RAW_IMG) $(COW_IMG)

boot-iso: 
	qemu-system-x86_64 -m $(MEMORY) \
		-cdrom ubuntu-14.04.2-server-amd64.iso 

boot-iso-disk: 
	qemu-system-x86_64 -m $(MEMORY) \
		-cdrom ubuntu-14.04.2-server-amd64.iso \
		-drive file=$(RAW_IMG),format=raw,if=scsi \
		-boot order=d

boot-iso-disk-kvm: 
	qemu-system-x86_64 -m $(MEMORY) \
		-cdrom ubuntu-14.04.2-server-amd64.iso \
		-enable-kvm \
		-drive file=$(RAW_IMG),format=raw,if=scsi \
		-boot order=d

#-smp [cpus=]n[,maxcpus=cpus][,cores=cores][,threads=threads][,sockets=sockets]
#set the number of CPUs to 'n' [default=1]
#maxcpus= maximum number of total cpus, including
#offline CPUs for hotplug, etc
#cores= number of CPU cores on one socket
#threads= number of threads on one CPU core
#sockets= number of discrete sockets in the system

boot-iso-disk-kvm-smp: 
	qemu-system-x86_64 -m $(MEMORY) \
		-cdrom ubuntu-14.04.2-server-amd64.iso \
		-enable-kvm \
		-smp 8,sockets=1,cores=8,threads=2 \
		-drive file=$(RAW_IMG),format=raw,if=scsi \
		-boot order=d

boot-ubuntu: 
	qemu-system-x86_64 -m $(MEMORY) \
		-boot order=c \
		-enable-kvm \
		-smp 8,sockets=1,cores=8,threads=2 \
		-drive file=$(UBUNTU_IMG),format=raw,if=scsi 

boot-ubuntu-virtio: 
	qemu-system-x86_64 -m $(MEMORY) \
		-boot order=c \
		-enable-kvm \
		-smp 8,sockets=1,cores=8,threads=2 \
		-drive file=$(UBUNTU_IMG),format=raw,if=virtio
have-vmx:
ifeq ($(VMX_SUPPORT),)
	@echo "No vmx support detected"
	@false
else
	@echo "vmx support detected"
endif
